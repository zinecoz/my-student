package com.zinecoz.springboot.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="classroom", schema="app")
public class Classroom {
	
	@Id
	@Column(name = "id", updatable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
	public Long id;
	
	@Column(name = "name")
	public String name;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "school_id", referencedColumnName = "id")
	public School school;
	
	@OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL, mappedBy = "classroom")
	public List<StudentClassroom> studentClassroom;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<StudentClassroom> getStudentClassroom() {
		return studentClassroom;
	}

	public void setStudentClassroom(List<StudentClassroom> studentClassroom) {
		this.studentClassroom = studentClassroom;
	}

}
