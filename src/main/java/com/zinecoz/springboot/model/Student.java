package com.zinecoz.springboot.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="student", schema="app")
public class Student {
	
	@Id
	@Column(name = "id", updatable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
	public Long id;
	
	@Column(name = "name")
	public String name;
	
	@Column(name = "email")
	public String email;
	
	@Column(name = "hp")
	public String hp;
	
	@Column(name = "student_id")
	public String studentId;
	
	@OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL, mappedBy = "student")
	public List<StudentClassroom> studentClassroom;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getHp() {
		return hp;
	}

	public void setHp(String hp) {
		this.hp = hp;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public List<StudentClassroom> getStudentClassroom() {
		return studentClassroom;
	}

	public void setStudentClassroom(List<StudentClassroom> studentClassroom) {
		this.studentClassroom = studentClassroom;
	}

}
