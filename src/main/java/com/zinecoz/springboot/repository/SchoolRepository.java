package com.zinecoz.springboot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.zinecoz.springboot.model.School;

public interface SchoolRepository extends JpaRepository<School, Long> {
	
	@Query(nativeQuery=true, value="SELECT * FROM app.school s  WHERE s.teacher_id=:id")
	List<School> findByTeacherId(@Param("id")Long id);

}
