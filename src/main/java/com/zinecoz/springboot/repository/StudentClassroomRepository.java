package com.zinecoz.springboot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.zinecoz.springboot.model.StudentClassroom;

public interface StudentClassroomRepository extends JpaRepository<StudentClassroom, Long> {
	
	@Query(nativeQuery=true, value="SELECT * FROM app.student_classroom s WHERE s.classroom_id=:id")
	List<StudentClassroom> findByClassroomId(@Param("id")Long id);
	
	@Query(nativeQuery=true, value="SELECT * FROM app.student_classroom s WHERE s.student_id=:id LIMIT 1")
	StudentClassroom findByStudentId(@Param("id")Long id);

}
