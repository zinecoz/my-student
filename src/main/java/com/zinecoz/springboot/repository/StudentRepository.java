package com.zinecoz.springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.zinecoz.springboot.model.Student;

public interface StudentRepository extends JpaRepository<Student, Long> {

}
