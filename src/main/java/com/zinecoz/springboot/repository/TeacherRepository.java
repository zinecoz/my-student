package com.zinecoz.springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.zinecoz.springboot.model.Teacher;

public interface TeacherRepository extends JpaRepository<Teacher, Long> {

}
