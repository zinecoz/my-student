package com.zinecoz.springboot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.zinecoz.springboot.model.Classroom;

public interface ClassroomRepository extends JpaRepository<Classroom, Long> {
	
	@Query(nativeQuery=true, value="SELECT * FROM app.classroom c WHERE c.school_id=:id")
	List<Classroom> findBySchoolId(@Param("id")Long id);

}
