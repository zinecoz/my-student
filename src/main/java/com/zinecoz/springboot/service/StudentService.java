package com.zinecoz.springboot.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zinecoz.springboot.model.Classroom;
import com.zinecoz.springboot.model.School;
import com.zinecoz.springboot.model.Student;
import com.zinecoz.springboot.model.StudentClassroom;
import com.zinecoz.springboot.model.Teacher;
import com.zinecoz.springboot.repository.ClassroomRepository;
import com.zinecoz.springboot.repository.SchoolRepository;
import com.zinecoz.springboot.repository.StudentClassroomRepository;
import com.zinecoz.springboot.repository.StudentRepository;
import com.zinecoz.springboot.repository.TeacherRepository;

@Service
@Transactional
public class StudentService {
	
	@Autowired
	private TeacherRepository teacherRepo;
	
	@Autowired
	private SchoolRepository schoolRepo;
	
	@Autowired
	private ClassroomRepository classRepo;
	
	@Autowired
	private StudentRepository studentRepo;
	
	@Autowired
	private StudentClassroomRepository studentClassRepo;
	
	public Teacher getTeacher(Long id) {
		return teacherRepo.getOne(id);
	}
	
	public School getSchoolById(Long id) {
		return schoolRepo.getOne(id);
	}
	
	public Classroom getClassroomById(Long id) {
		return classRepo.getOne(id);
	}
	
	public List<School> getSchoolByTeacher(Long id) {
		return schoolRepo.findByTeacherId(id);
	}
	
	public List<Classroom> getClassroomBySchool(Long id) {
		return classRepo.findBySchoolId(id);
	}
	
	public List<StudentClassroom> getStudentClassroomByClass(Long id) {
		return studentClassRepo.findByClassroomId(id);
	}
	
	public StudentClassroom getStudentClassroomByStudent(Long id) {
		return studentClassRepo.findByStudentId(id);
	}
	
	public void saveSchool( School sc, Long id ) {
		School sch = new School();
		Teacher th = teacherRepo.getOne(id);
		sch.setName(sc.getName());
		sch.setTeacher(th);
		schoolRepo.save(sch);
	}
	
	public void updateSchool( School sc, Long id ) {
		School sch = schoolRepo.getOne(id);
		sch.setName(sc.getName());
		schoolRepo.save(sch);
	}
	
	public void deleteSchool( Long id ) {
		schoolRepo.deleteById(id);
	}
	
	public void saveClassroom( Classroom cl, Long id ) {
		School sc = schoolRepo.getOne(id);
		Classroom cla = new Classroom();
		cla.setName(cl.getName());
		cla.setSchool(sc);
		classRepo.save(cla);
	}
	
	public void updateClassroom( Classroom cl, Long id ) {
		Classroom clh = classRepo.getOne(id);
		clh.setName(cl.getName());
		classRepo.save(clh);
	}
	
	public void deleteClassroom( Long id ) {
		classRepo.deleteById(id);
	}
	
	public void saveStudent( Student st, Long id ) {
		Classroom cl = classRepo.getOne(id);
		Student stu = new Student();
		stu.setEmail(st.getEmail());
		stu.setHp(st.getHp());
		stu.setName(st.getName());
		stu.setStudentId(st.getStudentId());
		studentRepo.save(stu);
		
		StudentClassroom stc = new StudentClassroom();
		stc.setClassroom(cl);
		stc.setStudent(stu);
		studentClassRepo.save(stc);
		
	}
	
	public void updateStudent( Student st, Long id ) {
		Student sth = studentRepo.getOne(id);
		sth.setName(st.getName());
		sth.setStudentId(st.getStudentId());
		sth.setHp(st.getHp());
		sth.setEmail(st.getEmail());
		studentRepo.save(sth);
	}
	
	public void deleteStudent( Long id ) {
		studentRepo.deleteById(id);
	}
	
	public void deleteStudentClass( Long id ) {
		studentClassRepo.deleteById(id);
	}

}
