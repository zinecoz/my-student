package com.zinecoz.springboot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.zinecoz.springboot.model.Classroom;
import com.zinecoz.springboot.model.School;
import com.zinecoz.springboot.model.Student;
import com.zinecoz.springboot.model.StudentClassroom;
import com.zinecoz.springboot.model.Teacher;
import com.zinecoz.springboot.service.StudentService;

@Controller
public class StudentController {
	
	@Autowired
	private StudentService service;
	
	@GetMapping("/home")
	public String home(Model model) {
		Teacher teacher = service.getTeacher((long) 1);
		model.addAttribute("teacher", teacher);
		model.addAttribute("mode", "home");
		return "home";
	}
	
	@RequestMapping(path = { "/school" })
	public String schoolList(Model model) {
		Teacher teacher = service.getTeacher((long) 1);
		List<School> school = service.getSchoolByTeacher(teacher.getId());
		School schoolNew = new School();
		model.addAttribute("teacher", teacher);
		model.addAttribute("school", school);
		model.addAttribute("schoolNew", schoolNew);
		model.addAttribute("mode", "master data");
		return "school_list";
	}
	
	@RequestMapping(path = { "/school/add", "/school/add/{id}" }, method = RequestMethod.POST)
	public String schoolAdd(@PathVariable(name = "id") long id, @ModelAttribute("schoolNew") School schoolNew) {
		service.saveSchool(schoolNew, id);
		return "redirect:/school/"+id;
	}
	
	@RequestMapping(path = { "/school/update", "/school/update/{id}" }, method = RequestMethod.POST)
	public String schoolUpdate(@PathVariable(name = "id") long id, @ModelAttribute("schoolNew") School schoolNew) {
		service.updateSchool(schoolNew, id);
		return "redirect:/school/"+id;
	}
	
	@RequestMapping(path = { "/school/delete", "/school/delete/{id}" }, method = RequestMethod.GET)
	public String schoolDelete(@PathVariable(name = "id") long id) {
		service.deleteSchool(id);
		return "redirect:/school/"+id;
	}
	
	@RequestMapping(path = { "/classroom/{id}" })
	public String classroomList(Model model, @PathVariable(name = "id") Long id) {
		School school = service.getSchoolById(id);
		List<Classroom> classroom = service.getClassroomBySchool(id);
		Classroom classroomNew = new Classroom();
		model.addAttribute("school", school);
		model.addAttribute("classroom", classroom);
		model.addAttribute("classroomNew", classroomNew);
		model.addAttribute("mode", "master data");
		return "classroom_list";
	}
	
	@RequestMapping(path = { "/classroom/add", "/classroom/add/{id}" }, method = RequestMethod.POST)
	public String classroomAdd(@PathVariable(name = "id") long id, @ModelAttribute("classroomNew") Classroom classroomNew) {
		service.saveClassroom(classroomNew, id);
		return "redirect:/classroom/"+id;
	}
	
	@RequestMapping(path = { "/classroom/update", "/classroom/update/{id}" }, method = RequestMethod.POST)
	public String classroomUpdate(@PathVariable(name = "id") long id, @ModelAttribute("classroomNew") Classroom classroomNew) {
		Classroom classroom = service.getClassroomById(id);
		service.updateClassroom(classroomNew, id);
		return "redirect:/classroom/"+classroom.getSchool().getId();
	}
	
	@RequestMapping(path = { "/classroom/delete", "/classroom/delete/{id}" }, method = RequestMethod.GET)
	public String classroomDelete(@PathVariable(name = "id") long id) {
		Classroom classroom = service.getClassroomById(id);
		service.deleteClassroom(id);
		return "redirect:/classroom/"+classroom.getSchool().getId();
	}
	
	@RequestMapping(path = { "/student/{id}" })
	public String studentList(Model model, @PathVariable(name = "id") Long id) {
		Classroom classroom = service.getClassroomById(id);
		List<StudentClassroom> studentClass = service.getStudentClassroomByClass(id);
		Student student = new Student();
		model.addAttribute("classroom", classroom);
		model.addAttribute("studentClass", studentClass);
		model.addAttribute("student", student);
		model.addAttribute("mode", "master data");
		return "student_list";
	}
	
	@RequestMapping(path = { "/student/add", "/student/add/{id}" }, method = RequestMethod.POST)
	public String studentAdd(@PathVariable(name = "id") long id, @ModelAttribute("student") Student student) {
		service.saveStudent(student, id);
		return "redirect:/student/"+id;
	}
	
	@RequestMapping(path = { "/student/update", "/student/update/{id}" }, method = RequestMethod.POST)
	public String studentUpdate(@PathVariable(name = "id") long id, @ModelAttribute("student") Student student) {
		StudentClassroom studentClass = service.getStudentClassroomByStudent(id);
		service.updateStudent(student, id);
		return "redirect:/student/"+studentClass.getClassroom().getId();
	}
	
	@RequestMapping(path = { "/student/delete", "/student/delete/{id}" }, method = RequestMethod.GET)
	public String studentDelete(@PathVariable(name = "id") long id) {
		StudentClassroom studentClass = service.getStudentClassroomByStudent(id);
		service.deleteStudentClass(studentClass.getId());
		service.deleteStudent(id);
		return "redirect:/student/"+studentClass.getClassroom().getId();
	}

}
